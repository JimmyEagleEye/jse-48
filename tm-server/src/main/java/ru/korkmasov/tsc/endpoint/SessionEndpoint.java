package ru.korkmasov.tsc.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.api.service.dto.ISessionRecordService;
import ru.korkmasov.tsc.api.service.dto.IUserRecordService;
import ru.korkmasov.tsc.api.service.model.ISessionService;
import ru.korkmasov.tsc.dto.SessionRecord;
import ru.korkmasov.tsc.dto.UserRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    private ISessionRecordService sessionRecordService;

    private IUserRecordService userService;

    private ISessionService sessionService;

    public SessionEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionRecordService sessionRecordService,
            @NotNull final IUserRecordService userService,
            @NotNull final ISessionService sessionService
    ) {
        super(serviceLocator);
        this.sessionRecordService = sessionRecordService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public SessionRecord open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {

        SessionRecord session = sessionRecordService.open(login, password);
        return session;
    }

    @WebMethod
    public void close(@WebParam(name = "session") final SessionRecord session) {
        serviceLocator.getSessionRecordService().validate(session);
        sessionRecordService.close(session);
    }

    @WebMethod
    public SessionRecord register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionRecordService.open(login, password);
    }

    @WebMethod
    public UserRecord setPassword(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserRecord updateUser(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
