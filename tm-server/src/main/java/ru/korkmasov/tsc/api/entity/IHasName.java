package ru.korkmasov.tsc.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
