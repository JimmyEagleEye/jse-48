package ru.korkmasov.tsc.api.repository.model;

import ru.korkmasov.tsc.api.IRepository;
import ru.korkmasov.tsc.model.UserGraph;

public interface IUserRepository extends IRepository<UserGraph> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final UserGraph user);

}
