package ru.korkmasov.tsc.service.model;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.model.AbstractGraph;

public abstract class AbstractGraphService<E extends AbstractGraph> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractGraphService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
