package ru.korkmasov.tsc.util;

import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void displayWelcome() {
        System.out.println("** WeLcOmE To TaSk MaNaGeR **");
    }


    static void incorrectValue() {
        System.out.println("Incorrect values");
    }

}

