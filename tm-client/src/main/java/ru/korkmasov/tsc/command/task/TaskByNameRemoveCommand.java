package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByNameRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskByName(serviceLocator.getSession(), name);
    }

}
