package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind";
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(serviceLocator.getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().bindTaskById(serviceLocator.getSession(), taskId, projectId);
    }
}
