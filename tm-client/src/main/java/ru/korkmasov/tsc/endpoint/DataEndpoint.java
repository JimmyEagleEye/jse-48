package ru.korkmasov.tsc.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tsc.korkmasov.ru/", name = "DataEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface DataEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataXmlJaxBRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataXmlJaxBResponse")
    @RequestWrapper(localName = "loadDataXmlJaxB", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataXmlJaxB")
    @ResponseWrapper(localName = "loadDataXmlJaxBResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataXmlJaxBResponse")
    public void loadDataXmlJaxB(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataBinRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataBinResponse")
    @RequestWrapper(localName = "saveDataBin", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataBin")
    @ResponseWrapper(localName = "saveDataBinResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataBinResponse")
    public void saveDataBin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataJsonRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataJsonResponse")
    @RequestWrapper(localName = "saveDataJson", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataJson")
    @ResponseWrapper(localName = "saveDataJsonResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataJsonResponse")
    public void saveDataJson(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataJsonJaxBRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataJsonJaxBResponse")
    @RequestWrapper(localName = "loadDataJsonJaxB", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataJsonJaxB")
    @ResponseWrapper(localName = "loadDataJsonJaxBResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataJsonJaxBResponse")
    public void loadDataJsonJaxB(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataXmlJaxBRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataXmlJaxBResponse")
    @RequestWrapper(localName = "saveDataXmlJaxB", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataXmlJaxB")
    @ResponseWrapper(localName = "saveDataXmlJaxBResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataXmlJaxBResponse")
    public void saveDataXmlJaxB(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataBinRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataBinResponse")
    @RequestWrapper(localName = "loadDataBin", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataBin")
    @ResponseWrapper(localName = "loadDataBinResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataBinResponse")
    public void loadDataBin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataYamlRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataYamlResponse")
    @RequestWrapper(localName = "saveDataYaml", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataYaml")
    @ResponseWrapper(localName = "saveDataYamlResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataYamlResponse")
    public void saveDataYaml(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataYamlRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataYamlResponse")
    @RequestWrapper(localName = "loadDataYaml", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataYaml")
    @ResponseWrapper(localName = "loadDataYamlResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataYamlResponse")
    public void loadDataYaml(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataXmlRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataXmlResponse")
    @RequestWrapper(localName = "saveDataXml", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataXml")
    @ResponseWrapper(localName = "saveDataXmlResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataXmlResponse")
    public void saveDataXml(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataJsonRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataJsonResponse")
    @RequestWrapper(localName = "loadDataJson", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataJson")
    @ResponseWrapper(localName = "loadDataJsonResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataJsonResponse")
    public void loadDataJson(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataBase64Request", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataBase64Response")
    @RequestWrapper(localName = "loadDataBase64", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataBase64")
    @ResponseWrapper(localName = "loadDataBase64Response", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataBase64Response")
    public void loadDataBase64(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataJsonJaxBRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataJsonJaxBResponse")
    @RequestWrapper(localName = "saveDataJsonJaxB", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataJsonJaxB")
    @ResponseWrapper(localName = "saveDataJsonJaxBResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataJsonJaxBResponse")
    public void saveDataJsonJaxB(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataXmlRequest", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/loadDataXmlResponse")
    @RequestWrapper(localName = "loadDataXml", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataXml")
    @ResponseWrapper(localName = "loadDataXmlResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.LoadDataXmlResponse")
    public void loadDataXml(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataBase64Request", output = "http://endpoint.tsc.korkmasov.ru/DataEndpoint/saveDataBase64Response")
    @RequestWrapper(localName = "saveDataBase64", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataBase64")
    @ResponseWrapper(localName = "saveDataBase64Response", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.SaveDataBase64Response")
    public void saveDataBase64(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );
}
